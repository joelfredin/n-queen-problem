// This solution will be based on two research articles.

#include<iostream>
#include<vector>
#include<string>

using namespace std;

/*
 Functions which creates two lists, one with all even numbers
 and the other with all odd numbers less than a given number
*/
vector<int> even_list(int size_of_board)
{
	vector<int> even_numbers;
	for (int i = 1; i < size_of_board + 1; i++)
	{
		if (i % 2 == 0)
		{
			even_numbers.push_back(i);
		}
	}
	return even_numbers;
}

vector<int> odd_list(int size_of_board)
{
	vector<int> odd_numbers;
	for (int i = 1; i < size_of_board + 1; i++)
	{
		if (i % 2 == 1)
		{
			odd_numbers.push_back(i);
		}
	}
	return odd_numbers;
}
/*
Creates three different lists which shuffles around some of the numbers
from even and odd lists. It outputs their concatenation.
*/
vector<int> modulo6(int size_of_board)
{
	vector<int> even_numbers = even_list(size_of_board);
	vector<int> odd_numbers = odd_list(size_of_board);
	even_numbers.insert(even_numbers.end(), odd_numbers.begin(), odd_numbers.end());
	return even_numbers;

}

vector<int> modulo6_modeq2(int size_of_board)
{
	vector<int> even_numbers = even_list(size_of_board);
	vector<int> odd_numbers = odd_list(size_of_board);

	odd_numbers.erase(odd_numbers.begin());
	odd_numbers.erase(odd_numbers.begin());
	odd_numbers.erase(odd_numbers.begin());
	odd_numbers.insert(odd_numbers.begin(), 1);
	odd_numbers.insert(odd_numbers.begin(), 3);
	odd_numbers.push_back(5);

	even_numbers.insert(even_numbers.end(), odd_numbers.begin(), odd_numbers.end());
	return even_numbers;

}

vector<int> modulo6_modeq3(int size_of_board)
{
	vector<int> even_numbers = even_list(size_of_board);
	vector<int> odd_numbers = odd_list(size_of_board);

	even_numbers.erase(even_numbers.begin());
	even_numbers.push_back(2);
	odd_numbers.erase(odd_numbers.begin());
	odd_numbers.erase(odd_numbers.begin());
	odd_numbers.push_back( 1);
	odd_numbers.push_back(3);

	even_numbers.insert(even_numbers.end(), odd_numbers.begin(), odd_numbers.end());
	return even_numbers;

}

/*
 Creates the chessboard. Different constructions depending on if
 the remainder of the size of the chessboard is 2, 3 or neither when
 taking modulo 6.
*/
vector<vector<int>> create_chessboard(int size_of_board)
{
	vector<int> row;
	vector<vector<int>> chessboard;
	if (!(size_of_board % 6 == 2) && !(size_of_board % 6 == 3))
	{

		for (int i = 0; i < modulo6(size_of_board).size(); i++)
		{
			for (int j = 1; j < modulo6(size_of_board).size() + 1; j++)
			{
				if (j == modulo6(size_of_board)[i])
				{
					row.push_back(modulo6(size_of_board)[i]);
				}
				else
				{
					row.push_back(0);
				}
			}
			chessboard.push_back(row);
			row.clear();
		}
	}
	if (size_of_board % 6 == 2)
	{

		for (int i = 0; i < modulo6_modeq2(size_of_board).size(); i++)
		{
			for (int j = 1; j < modulo6_modeq2(size_of_board).size() + 1; j++)
			{
				if (j == modulo6_modeq2(size_of_board)[i])
				{
					row.push_back(modulo6_modeq2(size_of_board)[i]);
				}
				else
				{
					row.push_back(0);
				}
			}
			chessboard.push_back(row);
			row.clear();
		}
	}
	if (size_of_board % 6 == 3)
	{

		for (int i = 0; i < modulo6_modeq3(size_of_board).size(); i++)
		{
			for (int j = 1; j < modulo6_modeq3(size_of_board).size() + 1; j++)
			{
				if (j == modulo6_modeq3(size_of_board)[i])
				{
					row.push_back(modulo6_modeq3(size_of_board)[i]);
				}
				else
				{
					row.push_back(0);
				}
			}
			chessboard.push_back(row);
			row.clear();
		}
	}
	return chessboard;
}

// Method used to visualize chessboard.
void visualize_board(vector<vector<int>> chess)
{
	for (vector<int> row : chess)
	{
		for (int i = 0; i < chess.size(); i++)
		{
			cout << row[i] << " ";
		}
		cout << endl;
	}
}

// Method which prints a 2x2 chessboard, with one piece placed at every row.

void chess_two_by_two()
{
	vector<vector<int>> chessboard = {{1,0},{0,2}};
	vector<vector<int>> chessboard2 = {{0,2},{1,0}};
	visualize_board(chessboard);
	cout << endl;
	visualize_board(chessboard2);
	cout << endl;
}

// Method which prints a 3x3 chessboard, with one piece placed at every row.
void chess_three_by_three()
{
	vector<vector<int>> chessboard = {{1,0,0},{0,2,0},{0,0,3}};
	for(int i = 0; i < chessboard.size(); i++)
	{
		chessboard[i] = {1,0,0};
		for(int j = 0; j < chessboard.size(); j++)
		{
			if(j == i)
			{
				continue;
			}
			chessboard[j] = {0,2,0};
			for(int k = 0; k<chessboard.size(); k++)
			{
				if(k == i || k == j)
				{
					continue;
				}
				chessboard[k] = {0,0,3};
				visualize_board(chessboard);
				cout << endl;
			}
		}
	}
}

int main()
{
	int size;
	cout << "Hello, and welcome to the wonderful world of N-Queen Puzzle. Spots marked with 0 are considered empty squares on the chessboard, while anything else is a queen.\n\nPlease enter a number of the size of the chessboard you want us to consider: " << endl;
	cin >> size;
	cout << endl;
	if(size == 1)
	{
		cout << "Here is the only valid chessboard of size 1" << endl;
		visualize_board({{1}});
	}
	if(size == 2)
	{
		cout << "Ahh, you entered the number 2, as you can see there are just two boards, and no one is a valid board." << endl;

		chess_two_by_two();
	}
	if(size == 3)
	{
		cout << "Ahh, you entered the number 3, as you can see there are six boards with a single queen placed at every row and column, each of which is not valid." << endl;
		chess_three_by_three();
	}
	if(size > 3)
	{
		cout << "Here is a valid board of size " + to_string(size) << "." << endl;
		vector<vector<int>> chessis = create_chessboard(size);
		visualize_board(chessis);
	}

	return 0;
}