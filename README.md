# Solution Finder of N-Queen Puzzle

## Projects Description
This program finds solutions to the N-Queen problem for N=2 up to N=10. It is an implementation of the mathematics explained in the following two articles:

## How to Use the Project
You use the project by simply running it. Everything works on it own.

## Credits

The code is based on the following two articles:

https://www.tandfonline.com/doi/abs/10.1080/0025570X.1969.11975924

https://dl.acm.org/doi/pdf/10.1145/122319.122322
